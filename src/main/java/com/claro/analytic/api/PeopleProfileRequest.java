package com.claro.analytic.api;

import com.claro.analytic.api.dto.FullNameData;
import com.claro.analytic.api.dto.HighestScrapingPriorityRequest;
import com.claro.analytic.api.dto.LinkedinScrapedSuccessfullyRequest;
import com.claro.analytic.api.dto.LinkedinStatusChangeRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Scope;
import org.springframework.data.util.Pair;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import static java.util.Objects.nonNull;

/**
 * API for management people profiles.
 */
@Component
@Scope("prototype")
@Slf4j
public class PeopleProfileRequest extends APIRequest {

    /**
     * PROFILE STATUS URL.
     */
    private static final String PROFILE_STATUS_URL
            = "/linkedinStatus/visibility?canonical=%s&status=%s";
    /**
     * PROFILE STATUS URL.
     */
    private static final String DELETE_SCRAPE_STATUS_URL
            = "/linkedinStatus/scrape?canonical=%s";

    /**
     * PROFILE STATUS URL.
     */
    private static final String CHANGE_LINKEDIN_STATUS_URI
            = "/linkedinStatus/change";
    /**
     * PROFILE STATUS URL.
     */
    private static final String LINKEDIN_SCRAPED_SUCCESSFULLY_URI
            = "/linkedinStatus/scrapedSuccessfully";

    /**
     * GENDER DIVERSITY URL.
     */
    private static final String GENDER_DIVERSITY_URL =
            "/fullName/genderDiversity/";

    /**
     * MARK PROFILE FOR HIGHEST SCRAPING PRIORITY URL.
     */
    private static final String MARK_HIGHEST_PRIORITY_URL =
            "/linkedinStatus/highestScrapingPriority";

    /** Rest template. */
    private final RestTemplate restTemplate = new RestTemplateBuilder().build();

    /**
     * Sends request to change profile scraping and/or visibility status.
     * @param request Status change request.
     */
    public void changeProfileStatus(final LinkedinStatusChangeRequest request) {
        sendStatusChangeRequest(CHANGE_LINKEDIN_STATUS_URI,
                request, request.getCanonical());
    }

    /**
     *
     * @param request
     */
    public void handleScrapedSuccessfully(
            final LinkedinScrapedSuccessfullyRequest request) {
        sendStatusChangeRequest(LINKEDIN_SCRAPED_SUCCESSFULLY_URI,
                request, request.getUrl());
    }

    /**
     * Marks profile for highest scraping priority.
     * @param request Request
     */
    public void markWithHighestScrapingPriority(
            final HighestScrapingPriorityRequest request) {
        String url = getApiHost() + MARK_HIGHEST_PRIORITY_URL;

        HttpEntity<HighestScrapingPriorityRequest> entity =
                new HttpEntity<>(request, getAuthHeaders());

        boolean completed = false;
        int tryCount = 0;
        final int maxTryCount = 3;
        while (!completed && tryCount < maxTryCount) {
            try {
                ResponseEntity<?> responseEntity = restTemplate
                        .exchange(url, HttpMethod.POST, entity, Object.class);
                log.info("response code {} from {} for {}",
                        responseEntity.getStatusCode(),
                        url,
                        request.getUrl());
                completed = true;
            } catch (HttpClientErrorException e) {
                log.error("response code " + e.getStatusCode()
                        + " from " + url
                        + " trying to set highest scraping priority for: "
                        + request.getUrl(), e);
                tryCount++;
                if (tryCount == maxTryCount) {
                    throw e;
                }
            }
        }
    }

    /**
     * Set profile status by canonical.
     * @param canonical
     * @param status
     */
    @Deprecated
    public void setProfileStatus(final String canonical, final String status) {
        String endPoint = getApiHost() + String.format(PROFILE_STATUS_URL,
                URLEncoder.encode(canonical, StandardCharsets.UTF_8), status);
        HttpURLConnection connection = null;
        try {
            URL url  = new URL(endPoint);
            connection = (HttpURLConnection) url
                    .openConnection();
            connection.setRequestMethod("PUT");
            connection.setRequestProperty("Authorization",
                    "Bearer " + getAuthService().getToken());
            int result = connection.getResponseCode();
            log.info("response code {} from {}", result, endPoint);
        } catch (Exception e) {
            log.error("error while set profile status: ", e);
        } finally {
            if (nonNull(connection)) {
                connection.disconnect();
            }
        }
    }

    /**
     * Delete scrape status from  profile by canonical.
     * @param canonical
     */
    @Deprecated
    public void deleteScrapeStatus(final String canonical) {
        String endPoint = getApiHost() + String.format(DELETE_SCRAPE_STATUS_URL,
                URLEncoder.encode(canonical, StandardCharsets.UTF_8));
        int responseCode = 0;
        int tryCount = 0;
        final int maxTryCount = 5;
        while (responseCode != HttpStatus.OK.value()
                && responseCode != HttpStatus.BAD_REQUEST.value()) {
            HttpURLConnection connection = null;
            try {
                URL url  = new URL(endPoint);
                connection = (HttpURLConnection) url
                        .openConnection();
                connection.setRequestMethod("DELETE");
                connection.setRequestProperty("Authorization",
                        "Bearer " + getAuthService().getToken());
                responseCode = connection.getResponseCode();
                log.info("response code {} from {}", responseCode,
                        endPoint);
                tryCount++;
            } catch (Exception e) {
                log.error(Arrays.toString(e.getStackTrace()));
            } finally {
                if (nonNull(connection)) {
                    connection.disconnect();
                }
            }
            if (tryCount > maxTryCount) {
                log.error(endPoint + " | " + responseCode);
                responseCode = HttpStatus.OK.value();
            }
        }
    }

    /**
     *
     * @param fullNames
     * @return pair
     */
    public Pair<HttpStatus, String> getGenderDiversity(
            final List<FullNameData> fullNames) {
        RestTemplate restTemplateLocal = new RestTemplate();

        try {
            String token = getAuthService().getToken();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", "Bearer " + token);
            String body = getObjectMapper().writeValueAsString(fullNames);
            HttpEntity<String> request = new HttpEntity(body, headers);
            ResponseEntity<String> response = restTemplateLocal
                    .postForEntity(getApiHost() + GENDER_DIVERSITY_URL,
                            request, String.class, new Object[0]);
            return Pair.of(response.getStatusCode(), response.getBody());
        } catch (JsonProcessingException e) {
            log.error("error while request to claro analytics API", e);
            return Pair.of(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        } catch (HttpClientErrorException e) {
            log.error("error while request to claro analytics API", e);
            return Pair.of(e.getStatusCode(), e.getMessage());
        }
    }

    private <T> void sendStatusChangeRequest(final String uri,
                                             final T request,
                                             final String linkedinUrl) {
        String url = getApiHost() + uri;
        HttpEntity<T> entity =
                new HttpEntity<>(request, getAuthHeaders());

        boolean completed = false;
        int tryCount = 0;
        final int maxTryCount = 3;
        while (!completed && tryCount < maxTryCount) {
            try {
                ResponseEntity<?> responseEntity = restTemplate
                        .exchange(url, HttpMethod.PUT, entity, Object.class);
                log.info("response code {} from {} for {}",
                        responseEntity.getStatusCode(),
                        url,
                        linkedinUrl);
                completed = true;
            } catch (HttpClientErrorException e) {
                log.error("response code " + e.getStatusCode() + " from " + url
                        + "for: " + linkedinUrl, e);
                if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
                    throw new RuntimeException(
                            "Unable to change profile status",
                            e
                    );
                }
                tryCount++;
            }
        }
    }

    private HttpHeaders getAuthHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(getAuthService().getToken());
        return headers;
    }
}
