package com.claro.analytic.api.scrapingStatusResolver;

import com.claro.analytic.api.ProdAnalyticsAPI;
import com.claro.analytic.api.TestAnalyticsAPI;
import com.claro.analytic.api.dto.HighestScrapingPriorityRequest;
import com.claro.analytic.api.dto.LinkedinScrapedSuccessfullyRequest;
import com.claro.analytic.api.dto.LinkedinStatusChangeRequest;
import com.claro.analytic.api.dto.LinkedinStatusChangeRequest.LinkedinStatusChangeRequestBuilder;
import com.pubdir.linkedin.AbstractLinkedinDocument;
import com.pubdir.linkedin.ScrapingStatus;
import com.pubdir.linkedin.VisibilityStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ScrapingStatusResolver {
    /** Analytics API for managing Claro test version. */
    private final TestAnalyticsAPI testAnalyticsAPI;

    /** Analytics API for managing Claro prod version. */
    private final ProdAnalyticsAPI prodAnalyticsAPI;

    /**
     * Requests needed changes for profile to make it not public.
     * @param linkedin Linkedin profile
     */
    public void handleProfileNotPublic(
            final AbstractLinkedinDocument linkedin
    ) {
        persistStatus(linkedin.getCanonical(),
                Optional.of(ScrapingStatus.NONE),
                Optional.of(VisibilityStatus.NOT_PUBLIC));
    }

    /**
     * Requests needed changes for profile to make it deleted.
     * @param linkedin Linkedin profile
     */
    public void handleProfileDeleted(
            final AbstractLinkedinDocument linkedin
    ) {
        persistStatus(linkedin.getCanonical(),
                Optional.of(ScrapingStatus.NONE),
                Optional.of(VisibilityStatus.DELETED));
    }

    /**
     * Changes profile statuses for successfully scraped profile.
     * @param linkedin Linkedin profile
     */
    public void handleScrapedSuccessfully(
            final AbstractLinkedinDocument linkedin
    ) {
        LinkedinScrapedSuccessfullyRequest request =
                new LinkedinScrapedSuccessfullyRequest(linkedin.getCanonical());

        testAnalyticsAPI.peopleProfiles().handleScrapedSuccessfully(request);
        prodAnalyticsAPI.peopleProfiles().handleScrapedSuccessfully(request);
    }

    /**
     * Marks url with highest priority scraping status.
     * @param url Profile url
     */
    public void markWithHighestScrapingPriority(final String url) {
        HighestScrapingPriorityRequest request =
                new HighestScrapingPriorityRequest();
        request.setUrl(url);
        prodAnalyticsAPI.peopleProfiles()
                .markWithHighestScrapingPriority(request);
    }

    /**
     * Removes scraping status.
     * @param linkedin Linkedin profile
     */
    public void removeScrapingStatus(
            final AbstractLinkedinDocument linkedin
    ) {
        persistStatus(linkedin.getCanonical(),
                Optional.of(ScrapingStatus.NONE), Optional.empty());
    }

    /**
     * Removes visibility status.
     * @param linkedin Linkedin profile
     */
    public void removeVisibilityStatus(
            final AbstractLinkedinDocument linkedin
    ) {
        persistStatus(linkedin.getCanonical(),
                Optional.empty(), Optional.of(VisibilityStatus.NONE));
    }

    private void persistStatus(
            final String canonical,
            final Optional<ScrapingStatus> scrapingStatus,
            final Optional<VisibilityStatus> visibilityStatus
    ) {
        LinkedinStatusChangeRequestBuilder builder =
                LinkedinStatusChangeRequest.builder();
        builder.canonical(canonical);
        visibilityStatus.ifPresent(builder::visibilityStatus);
        scrapingStatus.ifPresent(builder::scrapingStatus);
        LinkedinStatusChangeRequest request = builder.build();

        testAnalyticsAPI.peopleProfiles().changeProfileStatus(request);
        prodAnalyticsAPI.peopleProfiles().changeProfileStatus(request);
    }

    private boolean shouldRemoveScrapingStatus(
            final AbstractLinkedinDocument linkedin
    ) {
        String statusString = linkedin.getScrapingStatus();
        return ScrapingStatus.SCRAPE_ONCE.getOldStatus().equals(statusString)
                || ScrapingStatus.SCRAPE_FOR_MERGE_TOOL
                .getOldStatus().equals(statusString)
                || ScrapingStatus.SCRAPE_WITH_HIGHEST_PRIORITY
                .getOldStatus().equals(statusString);
    }

    private boolean shouldMakePublicAfterScrape(
            final AbstractLinkedinDocument linkedin
    ) {
        return VisibilityStatus.NEW_EMPTY.getOldStatus()
                .equals(linkedin.getVisibilityStatus());
    }
}


