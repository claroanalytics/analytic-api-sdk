package com.claro.analytic.api;

import com.claro.analytic.api.dto.BarChartAggregation;
import com.claro.analytic.api.dto.HeatMapAggregation;
import com.claro.analytic.api.dto.PieChartAggregation;
import com.claro.analytic.api.dto.UserSearches;
import com.pubdir.smartrecruiters.UpdateChargeResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * API for claro analytics chart requests.
 */
@Component
@Scope("prototype")
@Slf4j
public class ChartRequest extends APIRequest {
    /**
     * Claro API chart bar skills URL.
     */
    private static final String BAR_DATA_SKILLS_URL
            = "/supplyData/barData/skills";
    /**
     * Claro API heat map utl.
     */
    private static final String HEAT_MAP_URL
            = "/supplyData/heatMapData/1";
    /**
     * Claro API line graph url.
     */
    private static final String LINE_GRAPH_URL
            = "/supplyData/activeLogProc/30";
    /**
     * Claro API pie data cbsa url.
     */
    private static final String PIE_DATA_CBSA_URL
            = "/supplyData/pieData/cbsa";

    /**
     * Claro API chart compare bar skills URL.
     */
    private static final String BAR_DATA_LOCATION_URL
            = "/supplyData/barData/location";

    /**
     * Claro API company icons reset URL.
     */
    private static final String COMPANY_ICONS_RESET_URL
            = "/supplyData/employees-flow/icons/reset";
    /**
     * Claro API chart compare bar skills URL.
     */
    private static final String BAR_DATA_COMPARE_SKILLS_URL
            = "/chartToCompare/barData/skills";

    /**
     * Claro API chart compare bar skills URL.
     */
    private static final String DOMO_ACTIVE_LOGS_URL
            = "/domo/userActiveLogs";

    /**
     * Claro API chart compare bar skills URL.
     */
    private static final String DOMO_ACTIVE_STATUS_URL
            = "/domo/activeStatus";

    /**
     * Claro API chart supply current company position URL.
     */
    private static final String BAR_DATA_SUPPLY_CURRENT_COMPANY_POSITION_URL
            = "/supplyData/barData/current-company-position";

    /**
     * Claro API chart demand job position URL.
     */
    private static final String BAR_DATA_DEMAND_JOB_POSITION_URL
            = "/demandData/barData/position";

    /**
     * Claro API chart supply list data URL.
     */
    private static final String SUPPLY_LIST_DATA_URL
            = "/supplyData/listDataApi/%s";

    /**
     * get bar data skills.
     * @param json
     * @param compare
     * @return BarChartAggregation[]
     */
    public BarChartAggregation[] getBarDataSkills(final String json,
            final boolean compare) {
        return postRestData(compare ? BAR_DATA_COMPARE_SKILLS_URL
                        : BAR_DATA_SKILLS_URL, json,
                BarChartAggregation[].class);
    }

    /**
     * get bar data location.
     * @param json
     * @return BarChartAggregation[]
     */
    public BarChartAggregation[] getBarDataLocation(final String json) {
        return postRestData(BAR_DATA_LOCATION_URL, json,
                BarChartAggregation[].class);
    }

    /**
     * Get bar data supply current company position.
     * @param userSearches
     * @return BarChartAggregation[]
     */
    public BarChartAggregation[] getBarDataSupplyCurrentCompanyPosition(
            final UserSearches userSearches
    ) {
        return postRestData(
                BAR_DATA_SUPPLY_CURRENT_COMPANY_POSITION_URL,
                userSearches,
                BarChartAggregation[].class
        );
    }

    /**
     * Get bar data demand job position.
     * @param userSearches
     * @return BarChartAggregation[]
     */
    public BarChartAggregation[] getBarDataDemandJobPosition(
            final UserSearches userSearches
    ) {
        return postRestData(
                BAR_DATA_DEMAND_JOB_POSITION_URL,
                userSearches,
                BarChartAggregation[].class
        );
    }

    /**
     * Company icons reset.
     *
     */
    public void companyIconsReset() {
        postRestData(COMPANY_ICONS_RESET_URL, "{}", Void.class);
    }

    /**
     * get het map aggregation.
     * @param json
     * @return HeatMapAggregation
     */
    public HeatMapAggregation getHeatMapAggregation(final String json) {
        return postRestData(HEAT_MAP_URL, json, HeatMapAggregation.class);
    }

    /**
     * get Domo ActiveStatus.
     * @param json
     * @return list of maps
     */
    public List<Map<String, String>> getDomoActiveStatus(final String json) {
        return postRestData(DOMO_ACTIVE_STATUS_URL, json, List.class);
    }

    /**
     * get Domo ActiveLogs.
     * @param json
     * @return map
     */
    public Map<String, List<Map<String, Integer>>> getDomoActiveLogs(
            final String json) {
        return postRestData(DOMO_ACTIVE_LOGS_URL, json, Map.class);
    }

    /**
     * get Line graph.
     * @param json
     * @return map
     */
    public Map<Object, Object> getLineGraph(
            final String json) {
        return postRestData(LINE_GRAPH_URL, json, Map.class);
    }

    /**
     * get Pie data CBSA.
     * @param json
     * @return map
     */
    public PieChartAggregation[] getPieDataCbsa(
            final String json) {
        return postRestData(PIE_DATA_CBSA_URL, json,
                PieChartAggregation[].class);
    }

    /**
     * get Supply list of prospects.
     * @param page page
     * @param userSearches user searches
     * @return map
     */
    public Map<String, Object> getSupplyListOfProspects(
            final String page, final UserSearches userSearches) {
        return postRestData(String.format(SUPPLY_LIST_DATA_URL, page),
                userSearches, Map.class);
    }

}
