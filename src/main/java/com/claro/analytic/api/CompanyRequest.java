package com.claro.analytic.api;

import com.claro.analytic.api.dto.SearchParameter;
import com.claro.analytic.api.dto.enums.ParameterType;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.elasticsearch.common.Strings;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Component
@Scope("prototype")
@Slf4j
public class CompanyRequest extends APIRequest {

    /**
     * Company data url.
     */
    private static final String COMPANY_DATA_URL
            = "/demandData/listData/1";

    /**
     * Getter.
     * @param name name
     * @param countryCode country code
     * @return pair
     */
    public Pair<HttpStatus, Integer> getVacancyCount(
            final String name,
            final String countryCode
    ) {
        RestTemplate restTemplate = new RestTemplate();

        try {
            SearchParameter companyNameSearchParameter = new SearchParameter(
                    "jobCompany",
                    ParameterType.TEXT.getName(),
                    List.of(name));
            List<SearchParameter> parameters = new ArrayList<>();

            parameters.add(companyNameSearchParameter);

            if (!Strings.isNullOrEmpty(countryCode)) {
                parameters.add(createAndOperator());
                parameters.add(new SearchParameter(
                        "jobCountry",
                        ParameterType.TEXT.getName(),
                        List.of(countryCode)
                ));
            }

            String token = getAuthService().getToken();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", "Bearer " + token);
            String body = getObjectMapper().writeValueAsString(
                    Map.of("searchParameters", parameters)
            );
            HttpEntity<String> request = new HttpEntity(body, headers);
            ResponseEntity<Map> response = restTemplate
                    .postForEntity(getApiHost() + COMPANY_DATA_URL,
                            request, Map.class);
            int count = (int) Objects.requireNonNull(response.getBody())
                    .get("count");

            return Pair.of(response.getStatusCode(), count);
        } catch (JsonProcessingException e) {
            log.error("error while request to claro analytics API", e);
            return Pair.of(HttpStatus.INTERNAL_SERVER_ERROR, 0);
        } catch (HttpClientErrorException e) {
            log.error("error while request to claro analytics API", e);
            return Pair.of(e.getStatusCode(), 0);
        }
    }

    private SearchParameter createAndOperator() {
        return new SearchParameter(
                "AND",
                ParameterType.OPERATOR.getName(),
                List.of("AND")
        );
    }


}
