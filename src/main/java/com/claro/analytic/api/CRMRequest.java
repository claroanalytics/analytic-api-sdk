package com.claro.analytic.api;

import com.claro.analytic.api.dto.CRMSearchRequest;
import com.claro.analytic.api.dto.CRMSearchResponse;
import com.pubdir.linkedin.PersonContact;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

import static java.lang.String.format;

/**
 * API for CRM management.
 */
@Component
@Scope("prototype")
@Slf4j
@RequiredArgsConstructor
public class CRMRequest extends APIRequest {

    /**
     * CRM SEARCH Results URL.
     */
    private static final String CRM_SEARCH_BY_ID_URL =
            "/crm/statistic/search-results";

    /**
     * CRM SEARCH Statistic URL.
     */
    private static final String CRM_SEARCH_STATISTIC_BY_ID_URL =
            "/crm/statistic";
    /**
     * CRM SEARCH Refresh URL.
     */
    private static final String CRM_SEARCH_REFRESH_BY_ID_URL =
            "/crm/statistic/refresh";

    /**
     * Reveal contact URL.
     */
    private static final String CRM_REVEAL_CONTACT =
            "/crm/contact/email/%s/";

    /**
     * Reveal contact.
     * @param talentId talentId
     * @param token token
     * @return person contact
     */
    public PersonContact revealContact(final String talentId,
                                       final String token) {
        return getRestData(format(
                CRM_REVEAL_CONTACT, talentId), PersonContact.class, token);
    }

    /**
     * Get search results.
     * @param request search request.
     * @return response
     */
    public CRMSearchResponse getSearchResult(final CRMSearchRequest request) {
        return getCRMSearchResponse(CRM_SEARCH_BY_ID_URL, request);
    }

    /**
     * Get search statistic.
     * @param request search request.
     * @return response
     */
    public CRMSearchResponse getSearchStatistic(
            final CRMSearchRequest request
    ) {
        return getCRMSearchResponse(CRM_SEARCH_STATISTIC_BY_ID_URL, request);
    }

    /**
     * Refresh searches.
     * @param requests searches request.
     * @return response
     */
    public CRMSearchResponse refreshSearches(
            final List<CRMSearchRequest> requests
    ) {
        return getCRMSearchesResponse(CRM_SEARCH_REFRESH_BY_ID_URL, requests);
    }

    /**
     * Refresh search.
     * @param request searches request.
     * @return response
     */
    public CRMSearchResponse refreshSearch(
            final CRMSearchRequest request
    ) {
        return refreshSearches(Collections.singletonList(request));
    }

    /**
     * Get search results.
     * @param url url
     * @param request search request.
     * @return response
     */
    private CRMSearchResponse getCRMSearchResponse(
            final String url,
            final CRMSearchRequest request
    ) {
        try {
            String body = postRestData(
                    url,
                    getObjectMapper().writeValueAsString(request),
                    String.class
            );
            return CRMSearchResponse.builder().body(body).build();
        } catch (Exception e) {
            log.error("", e);
            return CRMSearchResponse.builder()
                    .isError(true)
                    .errorMessage(e.getMessage())
                    .build();
        }
    }

    /**
     * Get search results.
     * @param url url
     * @param requests searches request.
     * @return response
     */
    private CRMSearchResponse getCRMSearchesResponse(
            final String url,
            final List<CRMSearchRequest> requests
    ) {
        try {
            String body = postRestData(
                    url,
                    getObjectMapper().writeValueAsString(requests),
                    String.class
            );
            return CRMSearchResponse.builder().body(body).build();
        } catch (Exception e) {
            log.error("", e);
            return CRMSearchResponse.builder()
                    .isError(true)
                    .errorMessage(e.getMessage())
                    .build();
        }
    }

}
