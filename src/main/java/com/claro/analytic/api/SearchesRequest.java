package com.claro.analytic.api;

import com.claro.analytic.api.dto.UserSearches;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
@Scope("prototype")
@Slf4j
public class SearchesRequest extends APIRequest {
    /**
     * Claro API searches URL.
     */
    private static final String SEARCHES_URL
            = "/searches";

    /**
     * Get searches.
     * @return user searches list
     */
    public UserSearches[] getSearches() {
        return getRestData(
                SEARCHES_URL,
                new HashMap<>(),
                UserSearches[].class
        );
    }

    /**
     * Get searches.
     * @param userId user id
     * @return user searches list
     */
    public UserSearches[] getSearches(final String userId) {
        return getRestData(
                SEARCHES_URL + "/" + userId,
                new HashMap<>(),
                UserSearches[].class
        );
    }
}
