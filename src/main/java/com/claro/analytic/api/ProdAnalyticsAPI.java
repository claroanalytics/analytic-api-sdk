package com.claro.analytic.api;

import com.claro.analytic.api.auth.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Prod Claro Analytics API.
 */
@Component
@Scope("prototype")
public class ProdAnalyticsAPI extends ClaroAnalyticsAPI {
    /**
     * Host for prod api.
     */
    @Value("${analytics.api.prod.host:}")
    private String host;

    /**
     * Auth service.
     */
    @Autowired
    private AuthService authService;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getApiUrl() {
        return host;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AuthService getAuthService() {
        return authService;
    }
}
