package com.claro.analytic.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LinkedinScrapedSuccessfullyRequest {
    /** Profile url. */
    private String url;
}

