package com.claro.analytic.api.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor
public class CRMSearchRequest {
    /**
     * DEFAULT_PAGE_SIZE.
     */
    private static final int DEFAULT_PAGE_SIZE = 10;

    /** Id. */
    private final String id;

    /**
     * Page num starts from 1.
     */
    private int page = 1;

    /**
     * Page size.
     */
    private int pageSize = DEFAULT_PAGE_SIZE;

    /**
     * SearchParameters.
     */
    private List<SearchParameter> searchParameters;
}
