package com.claro.analytic.api.dto;

import lombok.Builder;
import lombok.Data;

/**
 * CRM search response.
 */
@Data
@Builder
public class CRMSearchResponse {

    /**
     * Error.
     */
    private boolean isError = false;

    /**
     * Error message.
     */
    private String errorMessage;

    /**
     * Body.
     */
    private String body;
}
