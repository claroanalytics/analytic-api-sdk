package com.claro.analytic.api.dto;

import com.pubdir.linkedin.ScrapingStatus;
import com.pubdir.linkedin.VisibilityStatus;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LinkedinStatusChangeRequest {
    /** Linkedin url. */
    private String canonical;
    /** New scraping status. */
    private ScrapingStatus scrapingStatus;
     /** New visibility status. */
    private VisibilityStatus visibilityStatus;
}
