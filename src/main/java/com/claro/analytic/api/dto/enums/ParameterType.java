package com.claro.analytic.api.dto.enums;

import lombok.Getter;

@Getter
public enum ParameterType {

    /**
     * Text.
     */
    TEXT("text"),
    /**
     * Operator.
     */
    OPERATOR("operator");

    /**
     * Name.
     */
    private final String name;

    ParameterType(final String nameArg) {
        this.name = nameArg;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
