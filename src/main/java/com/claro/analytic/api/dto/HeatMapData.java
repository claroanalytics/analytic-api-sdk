package com.claro.analytic.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Represents a point on the map with number of documents at this point.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class HeatMapData {

    /**
     * Latitude.
     */
    @JsonProperty("lat")
    private String lat;

    /**
     * Longitude.
     */
    @JsonProperty("lon")
    private String lon;

    /**
     * Number of documents.
     */
    @JsonProperty("docCount")
    private Integer docCount;

}
