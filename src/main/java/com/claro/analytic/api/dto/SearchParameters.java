package com.claro.analytic.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Search parameters.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SearchParameters {

    /**
     * Id.
     */
    private String id;

    /**
     * Parent id.
     */
    private String parentId;

    /**
     * Parameter type.
     */
    private String parameterType;
    /**
     * Parameter key.
     */
    private String parameterKey;
    /**
     * Parameter value.
     */
    private List<String> parameterValue;

    /**
     * To string.
     * @return string
     */
    @Override
    public String toString() {
        return "SearchParameters{" + "parameterType='" + parameterType
                + ", parameterKey='" + parameterKey
                + ", parameterValue='" + parameterValue + '}';
    }
}
