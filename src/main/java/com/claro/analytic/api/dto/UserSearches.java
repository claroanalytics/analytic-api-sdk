package com.claro.analytic.api.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserSearches {
    /**
     * Id.
     */
    private String id;

    /**
     * Search parameters.
     */
    private List<SearchParameters> searchParameters;

    /**
     * Search type.
     */
    private String searchType;

    /**
     * To string.
     * @return string
     */
    @Override
    public String toString() {
        return "UserSearches{"
                + "id='" + id + '\''
                + ", searchParameters=" + searchParameters
                + '}';
    }
}
