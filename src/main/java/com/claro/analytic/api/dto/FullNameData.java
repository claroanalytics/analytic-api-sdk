package com.claro.analytic.api.dto;

import lombok.Data;

/**
 * Message format required to request claro gender diversity api.
 */
@Data
public class FullNameData {
    /**
     * Name.
     */
    private String name;

    /**
     * Surname.
     */
    private String surname;
}
