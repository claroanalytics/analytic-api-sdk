package com.claro.analytic.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Pie chart aggregation.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PieChartAggregation {

    /**
     * Key (skill name, university name, etc).
     */
    @JsonProperty("key")
    private String key;

    /**
     * Number of documents by the key.
     */
    @JsonProperty("docCount")
    private Integer docCount;

    /**
     * Percentage of documents by the key.
     */
    @JsonProperty("percentage")
    private Double percentage;
}
