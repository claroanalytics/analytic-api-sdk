package com.claro.analytic.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Class represents <property>-><docCount> aggregation,
 * For example: aggregation by employee' skills.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public final class BarChartAggregation {

    /**
     * Key (skill name, university name, etc).
     */
    @JsonProperty("key")
    private String key;

    /**
     * Number of documents by the key.
     */
    @JsonProperty("docCount")
    private Integer docCount;

}
