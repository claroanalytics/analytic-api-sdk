package com.claro.analytic.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class SearchParameter {

    /**
     * Key.
     */
    private String parameterKey;

    /**
     * Type.
     */
    private String parameterType;

    /**
     * Value.
     */
    private List<String> parameterValue;

}
