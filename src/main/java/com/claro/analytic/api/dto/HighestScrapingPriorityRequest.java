package com.claro.analytic.api.dto;

import lombok.Data;

@Data
public class HighestScrapingPriorityRequest {
    /** Profile url. */
    private String url;
}
