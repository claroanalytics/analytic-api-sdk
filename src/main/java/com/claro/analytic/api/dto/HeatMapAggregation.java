package com.claro.analytic.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * Represents payload coming from Claro API for heatmap.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class HeatMapAggregation {

    /**
     * Minimal value.
     */
    @JsonProperty("min")
    private Integer min;

    /**
     * Maximum value.
     */
    @JsonProperty("max")
    private Integer max;

    /**
     * Number of documents.
     */
    @JsonProperty("count")
    private Integer count;

    /**
     * Collection of location -> value for heatmap.
     */
    @JsonProperty("data")
    private List<HeatMapData> data;

}
