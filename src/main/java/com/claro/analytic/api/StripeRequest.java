package com.claro.analytic.api;

import com.pubdir.smartrecruiters.StripeStatus;
import com.pubdir.smartrecruiters.UpdateChargeResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

/**
 * API for claro analytics chart requests.
 */
@Component
@Scope("prototype")
@Slf4j
public class StripeRequest extends APIRequest {

    /**
     * Claro API stripe charge URL.
     */
    private static final String SR_STRIPE_CHARGE_URL
            = "/stripe/subscription/charge";

    /**
     * Claro API stripe status URL.
     */
    private static final String SR_STRIPE_STATUS_URL
            = "/stripe/status/";

    /**
     * post stripe charge.
     * @param host host
     * @param token token
     * @return UpdateChargeResponse
     */
    public UpdateChargeResponse postStripeCharge(
            final String host, final String token) {
        return requestRestData(host, HttpMethod.POST, SR_STRIPE_CHARGE_URL,
                null, UpdateChargeResponse.class, token);
    }

    /**
     * get stripe status.
     * @param host host
     * @param token token
     * @param groupId groupId
     * @return StripeStatus
     */
    public StripeStatus getStripeStatus(
            final String host, final String token, final String groupId) {
        return requestRestData(host, HttpMethod.GET,
                SR_STRIPE_STATUS_URL + groupId,
                null, StripeStatus.class, token);
    }
}
