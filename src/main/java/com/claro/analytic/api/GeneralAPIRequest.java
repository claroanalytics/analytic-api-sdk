package com.claro.analytic.api;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * General API request to analytics.
 */
@Component
@Scope("prototype")
@Slf4j
@RequiredArgsConstructor
public class GeneralAPIRequest extends APIRequest {

    /**
     * Execute general api request.
     * @param urlPath
     * @param params
     * @param clazz
     * @param <T>
     * @return object
     */
    public <T> T generalGetRequest(final String urlPath,
                                   final Map<String, Object> params,
                                   final Class<T> clazz) {
        return getRestData(urlPath, params, clazz);
    }

    public <T> T generalPostRequest(final String urlPath,
                                   final String json,
                                   final Class<T> clazz) {
        return postRestData(urlPath, json, clazz);
    }

}
