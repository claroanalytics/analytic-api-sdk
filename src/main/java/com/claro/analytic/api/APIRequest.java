package com.claro.analytic.api;

import com.claro.analytic.api.auth.AuthService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

/**
 * Base API request.
 */
@Component
@Scope("prototype")
public class APIRequest {

    /**
     * Auth Service.
     */
    @Autowired
    @Getter
    @Setter
    private AuthService authService;

    /**
     * ObjectMapper.
     */
    @Autowired
    @Getter
    private ObjectMapper objectMapper;

    /**
     * API host.
     */
    @Getter
    @Setter
    private String apiHost;

    /** Rest template. */
    private final RestTemplate restTemplate = new RestTemplateBuilder().build();

    /**
     * Make rest POST query to analytics and return data.
     * @param json json query
     * @param urlPath url path
     * @param clazz class to map result
     * @param <T> class type
     * @return t
     */
    protected <T> T postRestData(final String urlPath, final String json,
                                 final Class<T> clazz) {
        HttpHeaders headers = getHttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(json, headers);
        ResponseEntity<T> result = restTemplate
                .exchange(getApiHost() + urlPath, HttpMethod.POST,
                        entity, clazz);
        return result.getBody();
    }

    /**
     * Get request.
     * @param url url
     * @param clazz class
     * @param token token
     * @return T
     * @param <T>
     */
    protected  <T> T getRestData(final String url, final Class<T> clazz,
                                 final String token) {
        HttpHeaders headers;
        if (StringUtils.hasLength(token)) {
            headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setBearerAuth(token);
        } else {
            headers = getHttpHeaders();
        }
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<T> response = restTemplate.exchange(
                getApiHost() + url, HttpMethod.GET, entity, clazz);
        return response.getBody();
    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(authService.getToken());
        return headers;
    }

    /**
     * Make rest POST query to analytics and return data.
     * @param host host
     * @param method method
     * @param urlPath url path
     * @param json json query
     * @param clazz class to map result
     * @param <T> class type
     * @param token token
     * @return t
     */
    protected <T> T requestRestData(
            final String host, final HttpMethod method, final String urlPath,
            final String json, final Class<T> clazz, final String token) {
        HttpHeaders headers;
        if (StringUtils.hasLength(token)) {
            headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setBearerAuth(token);
        } else {
            headers = getHttpHeaders();
        }
        HttpEntity<String> entity = new HttpEntity<>(json, headers);
        ResponseEntity<T> result = restTemplate
                .exchange(host + urlPath, method,
                        entity, clazz);
        return result.getBody();
    }

    /**
     * Make rest POST query to analytics and return data.
     * @param body body
     * @param urlPath url path
     * @param clazz class to map result
     * @param <T> class type
     * @return t
     */
    protected <T> T postRestData(
            final String urlPath,
            final Object body,
            final Class<T> clazz
    ) {
        HttpHeaders headers = getHttpHeaders();
        String json;
        try {
            json = new ObjectMapper().writeValueAsString(body);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        HttpEntity<String> entity = new HttpEntity<>(json, headers);
        ResponseEntity<T> result = restTemplate
                .exchange(getApiHost() + urlPath, HttpMethod.POST,
                        entity, clazz);
        return result.getBody();
    }

    /**
     * Make rest POST query to analytics and return data.
     * @param params query params
     * @param urlPath url path
     * @param clazz class to map result
     * @param <T> class type
     * @return t
     */
    protected <T> T getRestData(
            final String urlPath,
            final Map<String, Object> params,
            final Class<T> clazz
    ) {
        HttpHeaders headers = getHttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(headers);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(getApiHost() + urlPath);
        params.forEach(builder::queryParam);
        ResponseEntity<T> result = restTemplate
                .exchange(builder.build().toUriString(), HttpMethod.GET,
                        entity, clazz);
        return result.getBody();
    }
}
