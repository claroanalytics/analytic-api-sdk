package com.claro.analytic.api;

import com.claro.analytic.api.auth.AuthService;
import com.claro.analytic.api.auth.TestAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Test Claro Analytics API.
 */
@Component
@Scope("prototype")
public class TestAnalyticsAPI extends ClaroAnalyticsAPI {
    /**
     * Host for test api.
     */
    @Value("${analytics.api.test.host:}")
    private String host;

    /**
     * Auth service.
     */
    @Autowired
    private TestAuthService testAuthService;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getApiUrl() {
        return host;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AuthService getAuthService() {
        return testAuthService;
    }

}
