package com.claro.analytic.api;

import com.claro.analytic.api.auth.AuthService;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Base Claro Analytics API.
 */
@Component
@Scope("prototype")
public class ClaroAnalyticsAPI {

    /**
     * PeopleProfileRequest provider.
     */
    @Autowired
    private ObjectProvider<PeopleProfileRequest> peopleProfileProvider;

    /**
     * ChartRequest provider.
     */
    @Autowired
    private ObjectProvider<ChartRequest> chartRequestObjectProvider;

    /**
     * CompanyRequest provider.
     */
    @Autowired
    private ObjectProvider<CompanyRequest> companyRequestProvider;

    /**
     * CRMRequest provider.
     */
    @Autowired
    private ObjectProvider<CRMRequest> crmRequestObjectProvider;

    /**
     * GeneralAPIRequest provider.
     */
    @Autowired
    private ObjectProvider<GeneralAPIRequest> generalAPIRequestObjectProvider;

    /**
     * Searches request provider.
     */
    @Autowired
    private ObjectProvider<SearchesRequest> searchesRequestObjectProvider;
    /**
     * Searches request provider.
     */
    @Autowired
    private ObjectProvider<StripeRequest> stripeRequestObjectProvider;

    /**
     * Host.
     */
    @Value("${analytics.api.host:}")
    private String host;

    /**
     * Auth service.
     */
    @Autowired
    private AuthService authService;

    /**
     * Get api url.
     * @return api hurl.
     */
    public String getApiUrl() {
        return host;
    }

    /**
     * Auth service.
     * @return auth service
     */
    public AuthService getAuthService() {
        return authService;
    }

    /**
     *
     * @return PeopleProfileRequest
     */
    public PeopleProfileRequest peopleProfiles() {
        PeopleProfileRequest request = peopleProfileProvider.getObject();
        configRequest(request);
        return request;
    }

    /**
     *
     * @return ChartRequest
     */
    public ChartRequest charts() {
        ChartRequest request = chartRequestObjectProvider.getObject();
        configRequest(request);
        return request;
    }

    /**
     *
     * @return CRMRequest
     */
    public CRMRequest crm() {
        CRMRequest request = crmRequestObjectProvider.getObject();
        configRequest(request);
        return request;
    }

    /**
     *
     * @return CompanyRequest
     */
    public CompanyRequest companies() {
        CompanyRequest request = companyRequestProvider.getObject();
        configRequest(request);
        return request;
    }

    /**
     * get GeneralAPIRequest.
     * @return GeneralAPIRequest
     */
    public GeneralAPIRequest generalAPIRequest() {
        GeneralAPIRequest request = generalAPIRequestObjectProvider.getObject();
        configRequest(request);
        return request;
    }

    /**
     * Get searches request.
     * @return searches request
     */
    public SearchesRequest searchesRequest() {
        SearchesRequest request = searchesRequestObjectProvider.getObject();
        configRequest(request);
        return request;
    }

    public StripeRequest stripeRequest() {
        StripeRequest request = stripeRequestObjectProvider.getObject();
        configRequest(request);
        return request;
    }

    private void configRequest(final APIRequest apiRequest) {
        apiRequest.setApiHost(getApiUrl());
        apiRequest.setAuthService(getAuthService());
    }

}
