package com.claro.analytic.api.auth;

import com.auth0.client.auth.AuthAPI;
import com.auth0.exception.Auth0Exception;
import com.auth0.json.auth.TokenHolder;
import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Payload;
import com.auth0.net.AuthRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Service for authorization.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public abstract class AuthService {

    /**
     * Client id.
     * @return id
     */
    protected abstract String getClientId();

    /**
     * Client secret.
     * @return secret
     */
    protected abstract String getClientSecret();

    /**
     * Claro audience.
     * @return audience
     */
    protected abstract String getClaroAudience();

    /**
     * Auth host.
     * @return host
     */
    protected abstract String getHost();

    /**
     * Token.
     */
    private String token;

    /**
     * Get token.
     *
     * @return token
     */
    public String getToken() {
        return isExpired() ? getNewToken() : token;
    }

    /**
     * Refresh and return new token.
     * @return token
     */
    private String getNewToken() {
        log.info("renewing claro analytics access token");
        token = "";
        AuthAPI auth = new AuthAPI(
                getHost(),
                getClientId(),
                getClientSecret()
        );
        AuthRequest request = auth.requestToken(getClaroAudience());
        try {
            TokenHolder holder = request.execute();
            token = holder.getAccessToken();
        } catch (Auth0Exception e) {
            log.error("error while renew token: ", e);
        }
        return token;
    }

    /**
     * Is token expired.
     * @return true if expired.
     */
    private boolean isExpired() {
        if (StringUtils.isEmpty(token)) {
            return true;
        }
        Payload payload = JWT.decode(token);
        Date now = new Date();
        return now.after(payload.getExpiresAt());
    }
}
