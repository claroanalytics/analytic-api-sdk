package com.claro.analytic.api.auth;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * Service for authorization.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class TestAuthService extends AuthService {

    /**
     * Client id.
     */
    @Value("${analytics.access.client.test.id:}")
    private String clientId;

    /**
     * Client secret.
     */
    @Value("${analytics.access.client.test.secret:}")
    private String clientSecret;
    /**
     * Claro analytics Audience.
     */
    @Value(value = "${analytics.access.auth0.test.audience:}")
    private String claroAudience;

    /**
     * Host.
     */
    @Value("${analytics.access.test.host:}")
    private String host;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getClientId() {
        return clientId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getClientSecret() {
        return clientSecret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getClaroAudience() {
        return claroAudience;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getHost() {
        return host;
    }
}
